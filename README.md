# IT Guideline
This repository contains guideline for IT standard technology & working process. All parties involved in project or maintenance should follows the guideline.
However, it is still possible that some adjustments was made due to specific needs. In such case, please consult project manager.

----------

## IT Development
 - [Technology guideline](/it-development/technology_guideline.md)
 - [Documentation guideline](/it-development/documentation_guideline.md)
 - [Source code management](/it-development/scm_guideline.md)
 - [Doing microservice](/it-development/microservice_guideline.md)
 - [Android development](/it-development/android_development_guideline.md)


----------

## New Terms?
  - [Markdown document](/general/markdown_guideline.md)