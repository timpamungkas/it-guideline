# About Markdown
## What is Markdown?
See explanation from [wikipedia](https://en.wikipedia.org/wiki/Markdown)

## Why Markdown?
  - Since it is pure text-based files, collaboration can be done using source code management (git). This includes:
    -  add/remove/change/merge contents from multiple users
    - tracking revision history
    - branching
    - better user access control
  - Formatting is not as difficult as using html, so non-technical person can also learn and use it easily
  - You can read it directly from browser

## Markdown Editor
Regardless it's simplicity, special syntax still required to properly format markdown document. In such case, this links might help:

  - [online markdown editor](https://stackedit.io/editor)
  - [markdown cheatsheet](https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet)

----------

[back to index](/README.md)