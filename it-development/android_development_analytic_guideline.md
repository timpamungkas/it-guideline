# Analytics
## Analytic Tools : Fabric
  - All Android application **MUST** use [fabric](http://www.fabric.io), with following products installed : [Crashlytics](https://fabric.io/kits/android/crashlytics) and [Answers](https://fabric.io/kits/android/answers)
  - Log  event with corresponding TAG (on android studio, type *logt* for shortcut on creating TAG)

## Installation
  - Ask PM / Technical Lead to get fabric account for development / test / production
  - Install fabric : see [here](https://docs.fabric.io/android/fabric/overview.html)
Tips : To comfort your life, install it as Android Studio plugin

## **!! IMPORTANT !!** -  Where to Log Event (and How)
  - Log user information for Crashlytics (see [here](https://fabric.io/kits/android/crashlytics/features))
  - Track event, coordinate with functional/technical lead, and see Fabric guideline [here](https://docs.fabric.io/android/answers/answers-events.html#event-types) for possible event types

### **!! MUST Log** : Activity created
You must log at end of onCreate() for each Android activities, to get analytics about which screen acessed most.  Log custom event to see which screen opened

    public class MainActivity extends AppCompatActivity {

        private static final String TAG = {REPLACE_WITH_CLASS_NAME};

        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            
            // set Fabric Answers & Crashlytics here
            Fabric.with(this, new Crashlytics());
            
            setContentView(R.layout.activity_main);

            ...
            ...
            ...

            // log create event at last line
            Answers.getInstance().logCustom(new CustomEvent(TAG + " onCreate() finished");
        }
    }

Back to [Android Development Guideline](/it-development/android_development_guideline.md)