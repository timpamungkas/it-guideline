# Android - Layout
  - ConstraintLayout is preferred. Use it whenever possible
  - Use google guideline for developing on multiple screen layout. Coordinate with PM / Technical Lead


Back to [Android Development Guideline](/it-development/android_development_guideline.md)