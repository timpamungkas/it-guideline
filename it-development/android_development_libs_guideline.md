#SDK & Library Used
## Android - SDK Version
  - minSdkVersion : coordinate with PM / Technical Lead, based on [Google Android distribution](https://developer.android.com/about/dashboards/index.html)
  - targetSdkVersion : latest API version


## Asynchronous HTTP Calls... AsyncTask or Other Library?
 - As the rule of thumb : **don't use AsyncTask** for REST client
 - Use [Retrofit](http://square.github.io/retrofit/) + GSONConverter for REST client


## Image Library
  - Use [Picasso](http://square.github.io/picasso/)


## Eliminate Boilerplate Code?
  - Tired typing **findViewById** or binding click listeners? Use [Butterknife](https://github.com/JakeWharton/butterknife)


## ORM or SQLite?
  - Use [GreenDAO](http://greenrobot.org/greendao/documentation/how-to-get-started/) for android ORM, instead of plain SQLite


Back to [Android Development Guideline](/it-development/android_development_guideline.md)