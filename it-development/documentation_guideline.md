# Documentation Guideline
## How To Use This Guideline
  - Click each document from list below for detailed information
  - Not all documents are required within project. Consult Project <anager for documents required for each project
  - Adjustment for document required and/or their content should be approved by Project Manager

----------

## Document List
  - [User Requirement](/it-development/documentation_guideline_ureq.md)
  - [Functional Design](/it-development/documentation_guideline_fd.md)
  - [Technical Design](/it-development/documentation_guideline_td.md)
  - [UAT Document](/it-development/documentation_guideline_uat.md)
  - [Deployment Guideline](/it-development/documentation_guideline_dg.md)
  - [REST API Document](/it-development/documentation_guideline_api.md)

----------

[back to index](/README.md)