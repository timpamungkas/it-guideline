# Documentation - REST API
## Content
For each rest endpoint, describe:

  - Method (GET / PUT / POST / etc)
  - Implementation Notes / Brief Description of REST endpoint
  - Endpoint parameters (all of them)

----------

## Who Should Approve
  - Technical Lead

----------

## Template
Use swagger template, make sure that you write contents above. 
If you need Swagger + Springfox tutorial, these are good resources:

  - Springfox [documentation](https://springfox.github.io/springfox/docs/snapshot/)
  - [Google](https://www.google.com/)
  - [Baeldung](http://www.baeldung.com/swagger-2-documentation-for-spring-rest-api)
  - A [blog](http://fizzylogic.nl/2015/07/29/quickly-generate-api-docs-for-your-spring-boot-application-using-springfox/)


----------

## Sample
See swagger [petstore example](http://petstore.swagger.io/).

----------

## Tools
Create REST API Documentation using [Swagger](http://swagger.io/). Also use [Springfox](http://springfox.github.io/springfox/) if the project depends on Spring technology stack.

----------

## **IMPORTANT!!!** Disable Swagger on Production Environment
Using [Swagger](http://swagger.io/) along with [springfox](http://springfox.github.io/) for documentation is so powerful. At the same time, you get basic [Postman](https://www.getpostman.com/) on your API documentation, but this is also **dangerous.**

Dangerous when you open your API documentation on production, while API docs should not be shared as so publicly. Irresponsible people might exploit your API, and maybe do some harm.

So, swagger docs (both json format and swagger-ui) **must be disabled on production environment**.

----------

[back to index](/it-development/documentation_guideline.md)