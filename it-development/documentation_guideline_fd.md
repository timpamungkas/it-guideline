# Documentation - Functional Design
## Content
  1. Document Identity
    - Document Author
    - Document Approval
    - Revision History
    - Terminology
  2. Overview
    - Background
    - Existing System
    - Project Scope
    - Project Limitation
    - References
  3. Functional Design
    - High Level Design
      - End-To-End Business Model
      - Use Case Model
    - Functionality (1...n)
      - Use Case Model
      - Business Rules
      - Use Other System
      - Impact To Other System
      - Screen Design
         - Screen Business Rule
         - Field Description
         - Action Control
  4. Detailed Design
  5. Attachment

----------

## Who Should Approve
  - Project Manager
  - Business User / Product Manager
  - Direct supervisor of Business User / Product Manager
  - Business Analyst
  - Direct supervisor of Business Analyst

----------

## Template
Download template

----------

## Sample
[Download sample](/it-development/docs/documentation-fd-sample.rar)

----------

## Tools
See [technology guide](/it-development/technology_guideline.md) under section **Development Tools & IDE**.
  
If you need access / license to tools, contact project manager.

----------

[back to index](/it-development/documentation_guideline.md)
