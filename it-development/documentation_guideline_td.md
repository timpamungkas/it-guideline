# Documentation - Technical Design
## Content
  1. Document Identity
    - Document Author
    - Document Approval
    - Revision History
  2. Technical Design
    - System Architecture
    - Entity Diagram

----------

## Approved By
  - Project Manager
  - System Analyst
  - Direct supervisor of System Analyst

----------

## Template
Download template

----------

## Sample
[Download sample](/it-development/docs/documentation-td-sample.rar)

----------

## Tools
Create Entity Diagram using [Oracle Data Modeller](http://www.oracle.com/technetwork/developer-tools/datamodeler/overview/index.html) for domain diagram.

----------

[back to index](/it-development/documentation_guideline.md)
