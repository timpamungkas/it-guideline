# Doing Microservice
## Technology
  - See [technology standard](/it-development/technology_guideline.md) for REST service
  - [Spring Cloud Netflix](https://cloud.spring.io/spring-cloud-netflix/)
  - When using [JHipster](https://jhipster.github.io/), follow [JHipster guide for microservice](https://jhipster.github.io/microservices-architecture/)

----------

## Architecture
![Architecture](/it-development/images/architecture.png)
  

When using JHipster, use [JHipster Gateway](https://jhipster.github.io/microservices-architecture/#gateway) (which is based on [Zuul](http://cloud.spring.io/spring-cloud-netflix/spring-cloud-netflix.html)) and [JHipster Service Registry](https://jhipster.github.io/microservices-architecture/#jhipster-registry) (which is based on [Eureka](http://cloud.spring.io/spring-cloud-netflix/spring-cloud-netflix.html))