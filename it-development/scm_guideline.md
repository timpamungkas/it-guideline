# Source Code Management
## Source Code Management
  - [Guideline for Git](/it-development/scm_guideline_git.md)
  - [Guideline for SVN](/it-development/scm_guideline_svn.md)

----------

## Java Guideline
  - [Guideline for Java](/it-development/scm_guideline_java.md)

----------

## Android Guideline
  - [Guideline for Java](/it-development/scm_guideline_android.md)

----------

## Database Guideline
  - [Guideline for Oracle / Other RDBMS](/it-development/scm_guideline_rdbms.md)
  - [Guideline for MongoDB](/it-development/scm_guideline_mongo.md)
  - [Guideline for Redis](/it-development/scm_guideline_redis.md)

----------

[back to index](/README.md)