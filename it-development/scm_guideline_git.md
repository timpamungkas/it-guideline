# Source Code Management - Git
## Prerequisites
This guideline assuming you familiar with git basic concepts :

  - clone / initialize a repository
  - commit / stash / push
  - update / fetch / pull
  - branch
  - pull request (merge request) / reset
  - resolve conflict

If you're not familiar with git, these links might help:

  - [Google](https://www.google.com/)
  - [Github](https://try.github.io/levels/1/challenges/1)
  - [Udacity](https://www.udacity.com/course/how-to-use-git-and-github--ud775)
  - [Atlassian](https://www.atlassian.com/git/tutorials/)

----------
## Files Saved on Git Source Code
### Allowed
  - source code & text-based configuration files 
  - documentation in form of [markdown](https://en.wikipedia.org/wiki/Markdown)
  - usually only following files: java, xml, yml, properties, txt, DockerFile, jenkins file, md

### Not Allowed
  - Any kind of binary (example : java jar library, compiled jar / war), except maven/gradle wrapper 
  - Documentations in form of Microsoft Offices or PDF. If you need a place for documentation, create separate git repository from source code repository

### Why Documentation is NOT ALLOWED?
Git nature is to checkout entire repository from root-level. Thus if you have 10 folders and 1.000 files on your git repository, 
cloning it will checkout those 1.000 files.
That means if you put any documents or binaries (which sometimes take up to megabytes), git flow process (clone, update, etc) will take
a very long time.
Since git mainly used for source code management, keep the size of source code repository as small as possible so it will speed up git flow.
Thus, large files (that take up to megabytes) MUST be on separate place from source code repository.

----------


## Git Tools
 
  - For source code management, we use on-premise [gitlab](http://gitlab.com/). Please ask project manager for gitlab access.
  - You can use [tortoisegit](https://tortoisegit.org/) for helpful Windows UI
  - For IDE Git plugin, please do some google by yourself

----------

## Branches
### "Main" branches
Following are the "main" branches. These branches should contains latest stable source code for each team.

  - `master` : for development team
  - `test` : for QA team
  - `production` : for latest production release

### Branches for Change Request / Bug Fix
Following are branches for development purpose:

  - `ticketnumber` : for development team, created for each:
    - change request 
    - bug fix request 

### General Rules
General rules are:

  - Push only successful build (can be compiled) to those branches
  - If you need to merge a branch change into another, create a pull request.

### Branch : `master`
  - used only by development team
  - On new project, this branch is the main development branch
  - After project live, this branch should only changed by pull request from `ticketnumber`, no direct commit / push to this branch
  - merge `master` into `test` if it is ready for QA deployment

### Branch : `test`

  - used only by QA team
  - this branch should only changed by pull request from `master`, no direct commit / push to this branch
  - merge `test` into `production` if it is ready for production deployment

### Branch : `production`
  - used only for stable release to production
  - this branch should only changed by pull request from `test`, no direct commit / push to this branch


### Branch : `ticketnumber`
  - if there is any change request or bug fix, create your own branch (named `ticketnumber`) and merge it later into `master`
  - when ticket closed, also delete corresponding `ticketnumber` branch

----------

## How To Prepare a Git Repository

 1. Initialize a repository
 2. Set visibility & user access as required
 3. By default, `master` branch will already be created by git
 4. Add a `.gitignore` file. Use proper template (e.g. *java* or *android*)
 5. Add [these lines](/it-development/docs/gitignore.rar) to .gitignore file
 6. Create branch `test`, & `production` from `master`
 7. Go to project settings > protected branches. Protect branch `master`, `test`, & `production` from deletion, but allow merge & push

----------
[back to index](/it-development/scm_guideline.md)

