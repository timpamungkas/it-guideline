# Source Code Guideline - Java

## Packaging
### Do
Package start with **`id.co.fifgroup`**

### Don't!
Package start with other than **`id.co.fifgroup`**, like **`com.fifgroup`** or **`com.yourcompany`**

----------

## Database Connection Properties
### Do
Externalize database connection on JNDI, so when database address changes, we don't need to recompile the source code or change some text properties.

### Don't!
Write database connection on xml, properties, any other text files, or directly on source code.

----------
[back to index](/it-development/scm_guideline.md)