# Database Guideline - MongoDB
## Prerequisites
This guideline assuming you familiar with MongoDB basic concepts :

  - collection
  - document
  - field
  - embedded document

If you're not familiar with MongoDB, these links might help:
  - [MongoDB official site](https://docs.mongodb.com/manual/)
  - [Google](http://www.google.com)

----------

## Naming Collections
### Do
 - Name begins with 3 letters module code. Example: **`TRA_`** for Training
 - Use plural nouns. Example: **`TRA_COURSES`**
 - Collection name should represents what data it kepts. Example: **`TRA_COURSE_HISTORIES`**

### Don't
Collection name such as:
  - **`COURSES`** (no module code prefix)
  - **`TRA_COURSE`** (not plural noun)
  - **`TRA_CR_HIS`** (not clear what data it kept)

----------
[back to index](/it-development/scm_guideline.md)