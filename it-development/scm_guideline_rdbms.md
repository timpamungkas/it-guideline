# Database Guideline - Oracle / Other RDBMS

## Naming Tables
### Do
 - Name begins with 3 letters module code. Example: **`SAM_`** for System Administration
 - Represents what data it kepts. Example: **`SAM_SYSTEM_PROPERTIES`**
 - Table which is not header-detail should be on plural noun. Example: **`SAM_USERS`**
 - Table which is header-detail should use suffix:
   - HDR for header. Example: **`SAM_MENU_HDR`**
   - DTL for detail. Example: **`SAM_MENU_DTL`**

### Don't
Table name such as:
  - **`USERS`** (no module code prefix)
  - **`SAM_MENUS`**, **`SAM_MENUS_CONTENT`** (master-detail without proper suffix)
  - **`SAM_USER`** (not plural noun)
  - **`SAM_SYS_PRP`** (not clear what data it kept)

----------
## Naming Views
### Do
  - Follow table naming rule, and add view name with suffix **`_V`**. Example: **`SAM_USERS_V`**, **`SAM_SYSTEM_PROPERTIES_V`**

### Don't
  - View name such as:
    - anything that does not follow table naming rules above
    - **`SAM_USERS`** (view without prefix **`_V`**)

----------
## Naming Sequences
### Do
  - Sequence for primary key of particular table is table name with prefix **`_S`**
  - Example:
    - **`SAM_USERS_S`** for table `SAM_USERS`, column `USER_ID` (primary key)
    - **`SAM_MENU_HDR_S`** for table `SAM_MENU_HDR`, column `MENU_HDR_ID` (primary key_

### Don't
  - Sequence name such as:
    - **`SAM_USERS_SEQ`** or **`SAM_USERSS`** (not using **`_S`** as suffix)
    - **`USERS_S`** for table `SAM_USER` (not using table name)

----------
## "Who" Columns
### Do
  - For each table, create fields that defines who & when create/update record:
    - **`created_by`** (user who creates row)
    - **`creation_date`** (timestamp for row creation: day-month-year-hour-minute-second)
    - **`last_update_by`** *user who last updates row)
    - **`last_update_date`** (timestamp for row last update: day-month-year-hour-minute-second)

### Don't
Not creating fields above

----------
## Flag or Status Column
### Do
  - For flag or status in table (something like `approval_flag` or `document_status`), use full & meaningful word for content. Example:
    - SAVED
    - SUBMIT
    - REJECT
    - APPROVED
    - PENDING

### Don't
  - Use unclear abbreviation for status. Something like:
    - S
    - R
    - A
    - 1
    - 2
    - 3

----------
[back to index](/it-development/scm_guideline.md)