# Source Code Management - SVN
## Prerequisites
This guideline assuming you familiar with svn basic concepts :

  - checkout / create a repository
  - commit, update 
  - branch
  - merge request
  - revert
  - resolve conflict

If you're not familiar with svn, following links might help:

  - [Google](https://www.google.com/)
  - [Tutorialspoint](https://www.tutorialspoint.com/svn/svn_basic_concepts.htm)

----------
## Files Saved on SVN Repository
### Allowed
  - source code & text-based configuration files 
  - documentations in form of Microsoft Offices or PDF

### Not Allowed
  - Any kind of binary (example : java jar library, compiled jar / war), except maven/gradle wrapper

### Why Documentation is ALLOWED?
Unlike Git, on SVN you can checkout partially. Thus if you have 10 folders and 1.000 files on your git repository, you can checkout only 3 folders as you like.
That means if you put any documents or binaries while you only need the source code, you can ignore the documentation folder.

----------


## SVN Tools
 
  - For source code management, we use on-premise SVN server. Please ask project manager for SVN access.
  - You can use [tortoisesvn](https://tortoisesvn.net/) for helpful Windows UI
  - For IDE SVN plugin, please do some google by yourself

----------

## Branches
### "Main" branches
Following are the "main" branches. These branches should contains latest stable source code for each team.

  - `trunk` : for development team
  - `test` : for QA team
  - `production` : for latest production release

### Branches for Change Request / Bug Fix
Due to SVN nature that make it harder for branch housekeeping (adding/deleting branch), you should not create new branch for each change request or bug fix. Consult technical lead for creating new branch.

### General Rules
General rules are:

  - Commit only successful build (can be compiled) to those branches

### Branch : `trunk`
  - used only by development team
  - merge `trunk` into `test` if it is ready for QA deployment

### Branch : `test`
  - used only by QA team
  - this branch should only changed by merge request from `trunk`, no direct change to this branch
  - merge `test` into `production` if it is ready for production deployment

### Branch : `production`
  - used only for stable release to production
  - this branch should only changed by merge request from `test`, no direct change to this branch

----------

## How To Prepare a SVN Repository
SVN repository will no longer be used, so there should be no new repository

----------


[back to index](/it-development/scm_guideline.md)