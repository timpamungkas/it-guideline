# Technology Guideline
## Why This Guideline Exists?
Due to vast array of technology and frameworks, but limited man power and knowledge, a team can't handle every technology.
To maximize technology usage, it will be better if we focused on some technologies that frequently used.

----------

## How To Use This Guideline?
  - Choose appropriate technology from this guideline, according to system requirement
  - Not all of technology stack in this guideline need be used on every single project
  - Consult Project Manager if you need a technology that has not listed in this guideline, or you feel that this guideline is not suitable

### Example 1
A project needs NoSQL Database. Thus when choosing among NoSQL databases (example : Cassandra, MongoDB, Oracle NoSQL), use NoSQL Database that mentioned in this Guideline. Thus, for NoSQL Database, the project should use MongoDB and not other products. However, since NoSQL database is adequate, an Oracle database is not needed.

### Example 2
According to this guideline, a mobile project should use native Android / Swift. However, due to some reasons, a hybrid technology was required. Thus, before choosing among hybrid technologies (example : Ionic, Xamarin, Titanium), consult Project Manager.

----------

## Technology Version
You should always use latest technology version, unless:

 - technology version is written explicitly on this guideline
 - maintain backward compatibility for legacy applications
 - technical difficulties that make latest technology implementation is not possible 
 - latest version needs license (in this case, consult project manager)

Usage of obsolete technology version can only be done with approval from technical lead.

----------

## Database
 - Oracle Enterprise
 - [MongoDB](https://www.mongodb.com/)
 - [Redis](http://redis.io/)

----------

## Middleware
 - [Oracle Java 8](http://www.oracle.com/technetwork/java/javase/downloads/index.html)
 - [Spring Boot](https://projects.spring.io/spring-boot/) and supported technology by Spring boot, as listed on [Spring Initializr](https://start.spring.io/). These are list of Spring technology we use:
     - REST using Spring MVC
     - Spring JPA with Hibernate & [Hazelcast](https://hazelcast.com/use-cases/caching/hibernate-second-level-cache/) as Hibernate 2nd level cache
     - Spring Data MongoDB
     - Spring Redis
     - Spring Security
     - Spring Cloud
 - [MyBatis](http://www.mybatis.org/mybatis-3/). Some applications might use only JPA with hibernate, only MyBatis, or mixed


----------


## Web Application

 - [ZK](http://zkoss.org/) with [zk-bootstrap](https://www.zkoss.org/zk-bootstrap/)
 - [Thymeleaf](http://www.thymeleaf.org/). When using thymeleaf, use bootstrap template : [Porto](https://themeforest.net/item/porto-responsive-html5-template/4106987), [Porto Admin](https://themeforest.net/item/porto-admin-responsive-html5-template/8539472), or both.

When develop internal systems (mainly used for internal company usage), using ZK is preferred. If you develop system for end-user which needs beautiful user interface, Thymeleaf with Porto is preferred


----------


## JHipster
When using Spring stack (with or without [Thymeleaf](http://www.thymeleaf.org/)), using [JHipster](http://jhipster.github.io/) is preferred. 


----------


## Application Server
 - [Glassfish](https://glassfish.java.net/)
 - [Tomcat](http://tomcat.apache.org/)
 - [Weblogic](http://www.oracle.com/technetwork/middleware/weblogic/overview/index-085209.html)


----------


## Mobile Application
 - Android. Use [guidance from android site](https://developer.android.com/about/dashboards/index.html) for Android distribution that should be used
 - Swift for iPhone
 - [Fabric](https://fabric.io) for additional library

----------

## Infrastructure
### Container
  - [Docker](http://docker.com/)

### PaaS
  - On-premise [Openshift](https://www.openshift.com/)

----------

## Collaboration Tools
### Source Code Management
  - For new applications, use on-premise [Gitlab](https://gitlab.com/)
  - For older / legacy applications, use existing SVN

### Continuous Integration
  - [Jenkins](https://jenkins.io/)

### Bug Tracker
  - On-premise [Bugzilla](https://www.bugzilla.org/)

### Collaboration Tools
  - Proprietary IT Service Management
  - [Trello](https://trello.com/)

----------

## Development Tools & IDE
### Documentation
#### General Tools
  - Microsoft Word or markdown ([what is markdown?](/general/markdown_guideline.md)) on git

#### Functional Diagram
  - [Moqups](http://moqups.com/)

#### Technical Diagram
  - [Oracle Data Modeller](http://www.oracle.com/technetwork/developer-tools/datamodeler/overview/index.html)

#### REST API Documentation
  - [Swagger](http://swagger.io/) 
  - Use [Swagger](http://swagger.io/) with [Springfox](http://springfox.github.io/) if you use spring stack as middleware

### Java Development
 - [Maven](http://maven.apache.org/). If you want to use [Gradle](https://gradle.org/), consult technical lead
 - Any IDE that support Maven, but using [Eclipse](http://eclipse.org/) is suggested

### Android Development
  - [Android Studio](https://developer.android.com/studio/index.html)

----------

[back to index](/README.md)